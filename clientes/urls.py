from django.contrib import admin
from django.urls import path, include
from .views import persons_list,persons_create,persons_update,persons_delete
from clientes import urls as clientes_url

urlpatterns = [

    path("list/",persons_list, name ='person_list'),
    path("create/",persons_create, name='person_create'),
    path("update/<int:id>",persons_update, name='person_update'),
    path("delete/<int:id>",persons_delete, name='person_delete'),
]